%Compile with mmc -O6 -E --linkage static --mercury-linkage static --intermod-opt A258581.m
:- module 'A258581'.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module float, int, list, math, string.
:- pred gpf(int::in,int::out) is semidet.
:- pred is_even(int::in) is semidet.
:- pred get_list(int::in,int::out,int::out,list(int)::out) is nondet.

:- pred factor(int::in, list(int)::out) is det.
factor(N, Factors) :-
    Limit = float.truncate_to_int(math.sqrt(float(N))),
	factor(N, 2, Limit, [], Unsorted),
    list.sort_and_remove_dups([1, N | Unsorted], Factors).
 
:- pred factor(int, int, int, list(int), list(int)).
:- mode factor(in,  in,  in,  in,        out) is det.
factor(N, X, Limit, !Accumulator) :-
    ( if X  > Limit 
          then true
          else ( if 0 = N mod X 
                     then !:Accumulator = [X, N / X | !.Accumulator]
                     else true ),
               factor(N, X + 1, Limit, !Accumulator) ).
 
:- func factor(int) = list(int).
%:- mode factor(in) = out is det.
factor(N) = Factors :- factor(N, Factors).


gpf(N,P):-  factor(N,L),
            reverse(L,L2),
	    gpf2(L2,P).

:- pred gpf2(list(int)::in,int::out) is semidet.
gpf2([X|XS],N):- if is_prime(X) 
                 then N=X
		 else gpf2(XS,N).

:- pred is_prime(int::in) is semidet.
is_prime(N):- factor(N,[1|[N]]).

is_even(N):- 0 = N mod 2.


get_list(0,1,1,[]).
get_list(1,2,1,[2]).
get_list(N,ODD,EVEN,[P|LIST2]):-
  get_list(N-1,ODD2,EVEN2,LIST2),
  (if is_even(N) then
    (gpf(ODD2+1,P),EVEN = EVEN2 * P,ODD=ODD2)
  else
    (gpf(EVEN2+1,P),ODD = ODD2 * P,EVEN=EVEN2)).
   
main(!IO) :-
  io.read_line_as_string(Result, !IO),
  ( if
      Result = ok(String),
      string.to_int(string.strip(String),N),
      N > -1,
      get_list(N,_,_,L),
      reverse(L,L2)
    then
      io.format("A258581(%d, [", [i(N)], !IO),
      io.write_list(L2, ",", io.write_int, !IO),
      io.write_string("])\n", !IO),
      main(!IO)
    else
      io.format("That isn't a number or result undefined\n", [], !IO),
      main(!IO)
  ).
  
:- end_module 'A258581'.