% https://oeis.org/A259444
% a(1)=2. a(n) = smallest number > a(n-1) such that for all m,r<n a(n) != a(m)^a(r).

:- module 'A259444'.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module int, list, string, math. 
:- pred is_ok2(int::in, list(int)::in, list(int)::in) is semidet.
:- pred is_ok(int::in, int::in, list(int)::in) is semidet.
:- pred is_ok3(int::in, list(int)::in) is semidet.
:- pred is_not_power(int::in,int::in,int::in) is semidet.
:- pred get_max(list(int)::in,int::out) is semidet.
:- pred get_list(int::in, list(int)::out) is nondet.
:- pred get_list2(int::in, list(int)::in,list(int)::out) is nondet.
:- pred funct2(int::in, int::out) is nondet.

is_not_power(N,X,Y) :- not(N = pow(Y,X)), not(N = pow(X,Y)).

is_ok(_,_,[]).
is_ok(N,Y,[X|XS]):- is_not_power(N,X,Y), is_ok(N,Y,XS).

is_ok2(_,_,[]).
is_ok2(_,[],_).
is_ok2(N,[Y],[X]):- is_not_power(N,X,Y).
is_ok2(N,[Y|YS], L):- is_ok(N,Y,L), is_ok2(N,YS,L).

is_ok3(N,L) :- is_ok2(N,L,L).

get_max([X|_],X).

get_list2(0,[],[]).
get_list2(1,[],[2]).
 
get_list2(X,L1,L2) :- if is_ok3(X,L1) then L2=[X|L1] else get_list2(X+1,L1,L2).
 
get_list(0,[]).
get_list(1,[2]).
get_list(SIZE,L) :- get_list(SIZE-1,L2), get_max(L2,X), get_list2(X+1,L2,L). 

funct2(N, X) :- get_list(N,L), get_max(L,X). 
 
main(!IO) :-
  io.read_line_as_string(Result, !IO),
  ( if
      Result = ok(String),
      string.to_int(string.strip(String),N),
      N > -1,
      get_list(N,L1),
      reverse(L1,L2)
    then
      io.format("A259444(%d, [", [i(N)], !IO),
      io.write_list(L2, ",", io.write_int, !IO),
      io.write_string("])\n", !IO),
      main(!IO)
    else
      io.format("That isn't a number or result undefined\n", [], !IO),
      main(!IO)
  ).
  
:- end_module 'A259444'.