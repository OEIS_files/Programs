```python
def gpf(n):
  return prime_factors(n)[-1]

def gpf2(n):
    return (factor(n)[-1])[0]

def gpfp(v):
    r=1
    for i in range(0,len(v)):
        r *= v[i]
    return gpf(1+r)
    
def prime(n):
    j=2
    for i in range(1,n):
        j=next_prime(j)
    return j

def is_in(n,v):
  l=len(v)
  t=False
  for i in range(0,l-1):
      if v[i]==n:
        return True
  return False

def gpfp(v):
    return gpf(1+prod(v))
    
def prod(v):
    r=1
    for i in range(0,len(v)):
        r *= v[i]
    return r
          
#from rosettacode.org
def list_powerset(lst):
    # the power set of the empty set has one element, the empty set
    result = [[]]
    for x in lst:
        # for every additional element in our set
        # the power set consists of the subsets that don't
        # contain this element (just take the previous power set)
        # plus the subsets that do contain the element (use list
        # comprehension to add [x] onto everything in the
        # previous power set)
        result.extend([subset + [x] for subset in result])
    return result