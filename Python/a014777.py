# coding=utf-8

import math as mt

from mpmath import *

pi1 = "314159265358979323846264338327950288419716939937510582097494459230"

maxK = 35
mp.dps = 100
mp.prec = 504

debug = True


# From https://en.wikipedia.org/wiki/Chudnovsky_algorithm
# Adapted

def getpi():
    global maxK
    maxK = mt.ceil(2.5 * maxK)
    mp.prec *= 2
    mp.dps *= 2
    disp = mp.dps - 5
    k1, m, l, x, s = mpf(6), mpf(1), mpf(13591409), mpf(1), mpf(13591409)
    a1 = mpf(545140134)
    a2 = mpf(-262537412640768000)
    a3 = mpf(426880)
    a4 = mpf(10005)
    a5 = mpf(12)
    a6 = mpf(16)
    for k in range(1, maxK + 1):
        m = (k1 ** 3 - (k1 * a6)) * m / k ** 3
        l += a1
        x *= a2
        s += m * l / x
        k1 += a5
    pi2 = a3 * a4.sqrt() / s
    ret = str(pi2)[:disp]
    ret = ret.replace('3.', '3')
    return ret


def A014777(wanted_values=1000):
    global pi1
    finished = False
    lst = []
    counter = 0
    while not finished:
        pi1 = getpi()
        for i in range(counter, wanted_values):
            p = pi1.find(str(i))
            if p != -1:
                lst.append(p)
            else:
                counter = i
                finished = False
                break
            finished = True
    return lst

