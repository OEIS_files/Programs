[Mercury](./Mercury )

[Pari/GP](https://oeis.org/wiki/User:Anders_Hellstr%C3%B6m/Pari)

[Python](./Python)

[Ruby](./Ruby )

[Mathematica](./Mathematica)

[Haskell](./Haskell)

[Matlab](./Matlab )





[CodeAbbey](http://www.codeabbey.com/index/user_profile/anders-hellstrm) (Python)

[CodeCademy](https://www.codecademy.com/AndersH3)

[Sphere Online Judge](http://www.spoj.com/users/andersh3/)




![](https://projecteuler.net/profile/andersh3.png) ![](http://www.codeabbey.com/index/user_banner/anders-hellstrm.png)