#Includes code from
#http://rosettacode.org/wiki/Prime_decomposition
#and http://johncarrino.net/blog/2006/08/11/powerset-in-ruby/

require "mathn"

def prime(a)
  if a == 2
    true
  elsif a <= 1 || a % 2 == 0
    false
  else
    divisors = (3..Math.sqrt(a)).step(2)
    divisors.none? { |d| a % d == 0 }
  end
end

def prime_factors(i)
  factors = []
  check = proc do |p|
    while(q, r = i.divmod(p)
          r.zero?)
      factors << p
      i = q
    end
  end
  check[2]
  check[3]
  p = 5
  while p * p <= i
    check[p]
    p += 2
    check[p]
    p += 4    # skip multiples of 2 and 3
  end
  factors << i if i > 1
  factors
end
 

 
def next_prime(n)
    r=n+1
    while not prime(r)
      r += 1
    end
    r
end

class Array
  def powerSet!
    return [[]] if empty?()
    f = shift()
    rec = powerSet!
    return rec + rec.collect {|i| [f] + i }
  end

  def powerSet
    return clone().powerSet!
  end
end

def prod_plus_one_(w) #For inner arrays.
  z=1
  w.each {|x| z *= x}
  prime_factors(z+1)
end

def prod_plus_one(v) #For outer array.
  z=[]
  v.each {|w| z += prod_plus_one_(w) }
  z.uniq
end

def factorize(v) #Factorize vector to list of primes
  z=[]
  v.each {|i| z += prime_factors(i)}
  z.uniq
end

def is_in(a,v)
    t=false
    v.each {|i|
    if i==a then
       t=true
       break
      end
    }
    t
end

def get_next(v)
    x=0
    a=v[-1]
    p=factorize(prod_plus_one(v.powerSet))
    while x==0        
        a=next_prime(a)
        if not is_in(a,p) then
          x=1
          break
        end
    end
    a 
end

def first(m)
  v=[2]
  (2..m).each { |i| v << get_next(v)}
  v
end

print first(11)