```ruby
def gpf(n)
  prime_factors(n)[-1]
end
def primes(limit)
  (enclose = lambda { |primes|
    primes.last.succ.upto(limit) do |trial_pri|
      if primes.none? { |pri| (trial_pri % pri).zero? }
        return enclose.call(primes << trial_pri)
      end
    end
    primes
  }).call([2])
end
--Code from http://johncarrino.net/blog/2006/08/11/powerset-in-ruby/
class Array
  def powerSet!
    return [[]] if empty?()
    f = shift()
    rec = powerSet!
    return rec + rec.collect {|i| [f] + i }
  end

  def powerSet
    return clone().powerSet!
  end
end

print [1,2,3,4,5].powerSet
#Based on http://rosettacode.org/wiki/Prime_decomposition
# Get prime decomposition of integer _i_.
# This routine is more efficient than prime_factors,
# and quite similar to Integer#prime_division of MRI 1.9.
def prime_factors(i)
  factors = []
  check = proc do |p|
    while(q, r = i.divmod(p)
          r.zero?)
      factors << p
      i = q
    end
  end
  check[2]
  check[3]
  p = 5
  while p * p <= i
    check[p]
    p += 2
    check[p]
    p += 4    # skip multiples of 2 and 3
  end
  factors << i if i > 1
  factors
end
```