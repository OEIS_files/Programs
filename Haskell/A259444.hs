--https://hackage.haskell.org/package/readline-1.0.1.0/docs/System-Console-Readline.html

module Main (
    main
) where

import Prelude
import System.Console.Readline

first 0 = []
first m = (getNext2(n):n)
   where n=first(m-1)

getNext2 :: [Int] -> Int
getNext2 [] = 2
getNext2 (x:xs) = getNext (x+1,x:xs)
  where getNext (n, xs)
      | ispowerNum (n, xs) = getNext (n+1, xs)
      | otherwise = n

ispower :: (Int , (Either Int [Int]), (Either Int [Int]))->Bool
ispower (n, Left m, Right (x:xs)) = ispower (n, Left m, Left x) || ispower (n, Left m, Right xs)
ispower (n, Left m, Right []) = False
ispower (n, Left m,Left s)=n==m^s
ispower (n, Right (x:xs),Right xxs ) = ispower (n, Left x,Right xxs) || ispower (n, Right xs, Right xxs)
ispower (n, Right [], Right xxs) = False

ispowerNum :: (Int, [Int]) -> Bool
ispowerNum (n, xs) = ispower (n,Right xs,Right xs)

readEvalPrintLoop :: IO ()
readEvalPrintLoop = do
   print("A259444 a(1)=2. For n>1, a(n) = smallest number > a(n-1) such that, for all m,r<n, a(n) != a(m)^a(r).")
   maybeLine <- readline "Enter number of values. "
   case maybeLine of
    Nothing     -> return () -- EOF / control-d
    Just "exit" -> return ()
    Just line -> do addHistory line
                    print(reverse(first(read(line))))
                    readEvalPrintLoop

main :: IO()
main = readEvalPrintLoop