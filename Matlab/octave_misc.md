Some useful functions:

 [list_primes](http://octave.sourceforge.net/octave/function/list_primes.html)

 [isprime](http://octave.sourceforge.net/octave/function/isprime.html)

 [primes](http://octave.sourceforge.net/octave/function/primes.html)

 [lcm](http://octave.sourceforge.net/octave/function/lcm.html)

 [factor](http://octave.sourceforge.net/octave/function/factor.html)
 
 [uint64](http://octave.sourceforge.net/octave/function/uint64.html)

from http://octave.sourceforge.net/functions_by_alpha.php. 

```octave
a[1] is a(1)
```

From http://rosettacode.org/wiki/Octave:

```octave
function fact(n);
    f = factor(n);	% prime decomposition
    K = dec2bin(0:2^length(f)-1)-'0';   % generate all possible permutations
    F = ones(1,2^length(f));	
    for k = 1:size(K)
      F(k) = prod(f(~K(k,:)));		% and compute products 
    end; 
    F = unique(F);	% eliminate duplicates
    printf('There are %i factors for %i.\n',length(F),n);
    disp(F);
  end;
```
