From https://reference.wolfram.com/language/:

<pre>Plot[Piecewise[{{x^2, x < 0}, {x, x > 0}}], {x, -2, 2}]</pre>

<pre>PrimeQ[13]=True</pre>

<pre>
Table[Prime[n], {n, 20}]={2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71}
</pre>

<pre>RecurrenceTable[{a[n + 1] == 3 a[n], a[1] == 7}, a, {n, 1, 10}]</pre>

<pre>{PrimeNu[2], PrimeNu[2 3], PrimeNu[2 3 5]}={1, 2, 3}</pre>

<pre>{PrimeOmega[2], PrimeOmega[2^2], PrimeOmega[2^2 3]}={1, 2, 3}</pre>

<pre>NextPrime[1000000]=1000003</pre>

<pre>PrimePi[10^9]=50847534</pre>


From http://rosettacode.org/wiki/Category:Mathematica :

<pre>
Subsets[n, {1, Infinity}]
Subsets[{1, 2, 3}, {1, Infinity}]={{1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}
</pre>

<pre>
IsPrime[n_Integer] := Block[{}, If[n <= 1, Return[False]];
  If[n == 2, Return[True]]; If[Mod[n, 2] == 0, Return[False]];
  For[k = 3, k <= Sqrt[n], k += 2, 
   If[Mod[n, k] == 0, Return[False]]];
  Return[True]]
</pre>

<pre>
FactorInteger[9999999999999999999999]={{3, 2}, {11, 2}, {23, 1}, {4093, 1}, {8779, 1}, {21649, 1}, {513239, 
  1}}
</pre>

<pre>
Manipulate[Null, {{value, 0}, InputField[Dynamic[value], Number] &}, 
 Row@{Button["increment", value++], 
   Button["random", 
    If[DialogInput[
      Column@{"Are you sure?", 
        Row@{Button["Yes", DialogReturn[True]], 
          Button["No", DialogReturn[False]]}}], 
     value = RandomInteger@10000], Method -> "Queued"]}]
</pre>
